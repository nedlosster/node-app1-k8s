NAMESPACE=dev-app

kubectl delete -n $NAMESPACE -f ingress.yaml
kubectl delete -n $NAMESPACE -k .
kubectl delete -n $NAMESPACE -f storage.yaml
