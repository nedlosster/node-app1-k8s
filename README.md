# node-app1-k8s

Шаблон приложения для k8s. C поддержкой среды разработки и деплоя в облако.

```
minikube start
minikube addons enable ingress

./install/build_cont.sh
./install/k8s/00-create-ns.sh
./install/k8s/01-deploy.sh
```

Потом добавить /etc/hosts:

```
echo $(minikube ip) nodejs-ws-prod.local >> sudo tee /etc/hosts
echo $(minikube ip) nodejs-ws-dev.local  >> sudo tee /etc/hosts
```

Будет открываться на
https://nodejs-ws-prod.local
и на 
https://nodejs-ws-dev.local

Но для второго варианта нужно руками запустить сервер в поде  nodejs-ws-dev-0 


Удаление приложения:

```
./install/k8s/02-destroy.sh

```