NAMESPACE=dev-app

kubectl apply -n $NAMESPACE -f storage.yaml
kubectl apply -n $NAMESPACE -k .
kubectl apply -n $NAMESPACE -f ingress.yaml
