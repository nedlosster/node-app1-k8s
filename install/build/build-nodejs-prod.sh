#!/bin/bash

TAG=${1:-0.0}

echo "build nodejs-prod:$TAG"

DIR=$(cd $(dirname $0)/../../ && pwd)
pushd $DIR > /dev/null

eval $(minikube -p minikube docker-env)
docker build -t mcn/nodejs-prod:$TAG --file Dockerfile_prod .

popd > /dev/null

