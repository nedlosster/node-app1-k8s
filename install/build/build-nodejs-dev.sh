#!/bin/bash

TAG=${1:-0.0}

echo "build nodejs-dev:$TAG"

DIR=$(cd $(dirname $0)/../../ && pwd)
pushd $DIR > /dev/null

eval $(minikube -p minikube docker-env)
docker build -t mcn/nodejs-dev:$TAG --file Dockerfile_prod .

popd > /dev/null
